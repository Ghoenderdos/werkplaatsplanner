<?php

namespace App\Form;

use App\Entity\Availability;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BatchAvailabilityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('branch')
            ->add('startWorkDate', DateType::class, array(
                'widget' => 'single_text',))
            ->add('endWorkDate', DateType::class, array(
                'widget' => 'single_text',))
            ->add('days', ChoiceType::class, array(
                'choices'  => array(
                    'Maandag' => 1,
                    'Dinsdag' => 2,
                    'Woensdag' => 3,
                    'Donderdag' => 4,
                    'Vrijdag' => 5,
                    'Zaterdag' => 6,
                    'Zondag' => 7
                ),
                'multiple' => true,
                'expanded' => true
            ))
            ->add('hours')
        ;
    }
}
