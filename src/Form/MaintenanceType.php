<?php

namespace App\Form;

use App\Entity\Maintenance;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class MaintenanceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Title', null, array(
                'label' => 'Titel'))
            ->add('Description', null, array(
                'label' => 'Beschrijving'))
            ->add('Duration', ChoiceType::class, array(
                'label' => 'Tijdsduur',
                'choices' => array(
                    '1 uur' => 1,
                    '2 uur' => 2,
                    '4 uur' => 4,
                    '8 uur' => 8
                )))
            ->add('Price', MoneyType::class, array(
                'label' => 'Prijs',
                'divisor' => 100));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Maintenance::class,
        ]);
    }
}