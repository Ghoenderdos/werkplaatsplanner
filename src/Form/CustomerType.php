<?php

namespace App\Form;

use App\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', null, array(
                'label' => 'Voornaam'))
            ->add('lastName', null, array(
                'label' => 'Achternaam'))
            ->add('namePrefix', null, array(
                'label' => 'Tussen voegsel'))
            ->add('street', null, array(
                'label' => 'Straat'))
            ->add('houseNumber', null, array(
                'label' => 'Huis nummer'))
            ->add('postalCode', null, array(
                'label' => 'Postcode'))
            ->add('phone', null, array(
                'label' => 'Telefoon nummer'))
            ->add('email', null, array(
                'label' => 'Email'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
        ]);
    }
}
